﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Inetlab.SMPP;
using Inetlab.SMPP.Common;
using Inetlab.SMPP.Logging;
using Inetlab.SMPP.PDU;
using log4net.Repository;

namespace LoggingWithLog4net
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Log4net configuration
            ILoggerRepository repository = log4net.LogManager.GetRepository();
            var fileInfo = new FileInfo(@"log4net.config");
            log4net.Config.XmlConfigurator.Configure(repository, fileInfo);

            // Log4net Factory
            LogManager.SetLoggerFactory(new Log4netFactoryAdapter());

            Console.WriteLine("*** Running with Log4net...");

            SendHelloWorld().Wait();

            Console.ReadLine();
        }

        // <SendHelloWorld>
        public static async Task SendHelloWorld()
        {

            using (SmppServer server = new SmppServer(new IPEndPoint(IPAddress.Any, 7777)))
            {
                server.evClientBind += (sender, client, data) => { }; // return always ESME_ROK
                server.evClientSubmitSm += (sender, client, data) => { }; // return always ESME_ROK

                await server.StartAsync();

                using (SmppClient client = new SmppClient())
                {

                    if (await client.ConnectAsync("localhost", 7777))
                    {
                        BindResp bindResp = await client.BindAsync("1", "2");

                        if (bindResp.Header.Status == CommandStatus.ESME_ROK)
                        {
                            var submitResp = await client.SubmitAsync(
                                SMS.ForSubmit()
                                    .From("111")
                                    .To("222")
                                    .Coding(DataCodings.UCS2)
                                    .Text("Hello World!"));

                            if (submitResp.All(x => x.Header.Status == CommandStatus.ESME_ROK))
                            {
                                client.Logger.Info("Message has been sent.");
                            }
                        }

                        await client.DisconnectAsync();
                    }
                }
            }
        }
        //</SendHelloWorld>
    }
}

