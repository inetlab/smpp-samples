﻿using System;
using System.Globalization;
using Inetlab.SMPP.Logging;
using log4net.Util;

/* 
 * Use app.config if you need to debug a failing log4net
 * */


namespace LoggingWithLog4net
{
    // ----- FactoryAdapter ------
    public class Log4netFactoryAdapter : ILogFactory
    {
        public Inetlab.SMPP.Logging.ILog GetLogger(string loggerName)
        {
            return new Log4netFactoryLogger(loggerName);
        }
    }
    // ----- FactoryLogger ------

    public class Log4netFactoryLogger : Inetlab.SMPP.Logging.ILog
    {
        private readonly log4net.ILog _internalLog;

        public Log4netFactoryLogger(string loggerName)
        {
            _internalLog = log4net.LogManager.GetLogger(loggerName);
        }

        public bool IsEnabled(Inetlab.SMPP.Logging.LogLevel level)
        {
            return _internalLog.Logger.IsEnabledFor(GetLevel(level));
        }

        public void Write(Inetlab.SMPP.Logging.LogLevel level, string message, Exception ex, params object[] args)
        {
            _internalLog.Logger.Log(null, GetLevel(level), new SystemStringFormat(CultureInfo.InvariantCulture, message, args), ex);
        }

        private static log4net.Core.Level GetLevel(Inetlab.SMPP.Logging.LogLevel level)
        {
            switch (level)
            {
                case LogLevel.All:
                case LogLevel.Verbose:
                    return log4net.Core.Level.Verbose;  // Trace
                case LogLevel.Debug:
                    return log4net.Core.Level.Debug;
                case LogLevel.Info:
                    return log4net.Core.Level.Info;
                case LogLevel.Warning:
                    return log4net.Core.Level.Warn;
                case LogLevel.Error:
                    return log4net.Core.Level.Error;
                case LogLevel.Fatal:
                    return log4net.Core.Level.Fatal;
                case LogLevel.Off:
                    return log4net.Core.Level.Off;
                default:
                    throw new ArgumentOutOfRangeException(nameof(level), level, null);
            }
        }
    }

}