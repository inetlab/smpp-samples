﻿using System;
using System.Net;
using System.Threading.Tasks;
using Inetlab.SMPP;
using Inetlab.SMPP.Common;
using Inetlab.SMPP.Logging;
using Inetlab.SMPP.PDU;

namespace SmppServerDemoConsole
{
    public class Program
    {
        private SmppServer _server;

        public static async Task Main()
        {
            LogManager.SetLoggerFactory(new ConsoleLogFactory(LogLevel.Info));

            Console.Title = "SmppServer Demo";

            Program p = new Program();
            await p.RunAsync();
            Console.ReadLine();
        }

        private Task RunAsync()
        {
            _server = new SmppServer(new IPEndPoint(IPAddress.Any, 7777));


            _server.evClientConnected += (sender, client) =>
            {

            };
            _server.evClientDisconnected += (sender, client) =>
            {

            };
            _server.evClientSubmitSm += WhenServerReceivesPDU;

            return _server.StartAsync();
        }

        private async void WhenServerReceivesPDU(object sender, SmppServerClient serverClient, SubmitSm data)
        {


            if (data.RegisteredDelivery == 1)
            {

                //Send Delivery Receipt when required
                await serverClient.DeliverAsync(
                    SMS.ForDeliver()
                        .From(data.SourceAddress)
                        .To(data.DestinationAddress)
                        .Coding(data.DataCoding)
                        .Receipt(new Receipt
                        {
                            DoneDate = DateTime.Now,
                            State = MessageState.Delivered,
                            MessageId = data.Response.MessageId,
                            ErrorCode = "0",
                            SubmitDate = DateTime.Now
                        }
                        )
                )
                .ConfigureAwait(false);
            }
        }

    }
}
