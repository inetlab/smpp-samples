﻿using System.Diagnostics;
using System.Reflection;
using System.Windows.Forms;

namespace SmppClientDemo
{

    public partial class AboutSmppClientDemo : Form
    {

        public AboutSmppClientDemo()
        {
            InitializeComponent();
            label1.Text = "Version: " + Assembly.GetExecutingAssembly().GetName().Version;

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ProcessStartInfo psi = new ProcessStartInfo("https://www.inetlab.com/Products/Inetlab.SMPP.html");
            psi.UseShellExecute = true;
            Process.Start(psi);
        }


    }
}
