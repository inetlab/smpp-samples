﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Inetlab.SMPP;
using Inetlab.SMPP.Common;
using Inetlab.SMPP.Logging;
using Inetlab.SMPP.PDU;

namespace TestLocalPerformance
{
    public class Program
    {
        public static async Task Main()
        {

            LogManager.SetLoggerFactory(new ConsoleLogFactory(LogLevel.Info));

            //allow to use more threads from the beginning
            ThreadPool.SetMinThreads(1000, 1000);

            await StartTest();

        }

        public static async Task StartTest()
        {

            using (SmppServer server = new SmppServer(new IPEndPoint(IPAddress.Any, 7777)))
            {
                server.evClientBind += (sender, client, data) => { /*accept all*/ };
                server.evClientSubmitSm += (sender, client, data) => {/*receive all*/ };
                await server.StartAsync();

                using (SmppClient client = new SmppClient())
                {


                    await client.ConnectAsync("localhost", 7777);

                    await client.BindAsync("username", "password");

                    int batchSize = 50000;



                    var batch = CreateBatch(client, batchSize);

                    Console.WriteLine($"Send {batchSize} messages.");

                    client.Metrics.Reset();

                    Stopwatch watch = Stopwatch.StartNew();

                    var responses = await client.SubmitAsync(batch);

                    watch.Stop();

                    var clientMetrics = client.Metrics.Snapshot();

                    Console.WriteLine($"Elapsed: {watch.ElapsedMilliseconds} ms");

                    Console.WriteLine($"Performance:      {(double)batchSize / watch.ElapsedMilliseconds * 1000} m/s");

                    Console.WriteLine($"Request sending:  {clientMetrics.Sent.Requests.AvgPerSecond} m/s");
                    Console.WriteLine($"Response receive: {clientMetrics.Received.Responses.AvgPerSecond} m/s");

                    Console.WriteLine($"Server response time: {clientMetrics.Sent.Requests.ResponseTime}");

                    Console.WriteLine(
                        $"Non-Ok Response Status {responses.Count(x => x.Header.Status != CommandStatus.ESME_ROK)}");

                }
            }
        }


        public static IReadOnlyList<SubmitSm> CreateBatch(SmppClient client, int messagesCount)
        {
            List<SubmitSm> requests = new List<SubmitSm>();

            for (int i = 0; i < messagesCount; i++)
            {
                requests.AddRange(SMS.ForSubmit().From("1111").To("2222").Text("test" + (i + 1)).Create(client));
            }

            return requests;
        }


    }
}
