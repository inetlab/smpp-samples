# SmppServerDemo

An SMPP server simulator with a GUI that supports multiple connections, sends back delivery receipts upon request, and can deliver DeliverSm PDUs to the client.
