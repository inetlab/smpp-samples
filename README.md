# Sample applications for Inetlab.SMPP library

## HelloWorld

Connects SmppClient to SMPP server and sends simple SMS.

## Logging

  
### LoggingWithLog4net

Output library log events to [log4net logging framework](https://logging.apache.org/log4net/). 

### LoggingWithMicrosoft

Output library log events to [Microsoft logging framework](https://docs.microsoft.com/en-us/dotnet/core/extensions/logging). 

### LoggingWithNLog

Output library log events to [NLog logging framework](https://nlog-project.org/). 

## Windows Forms Applications

### SmppClientDemo

Simple SMPP client GUI. Connect to SMPP server send single and batch SMS messages. Outputs log events to the textbox.

### SmppServerDemo

SMPP server simulator with GUI. Accepts multiple connections. Sends back delivery receipt if requested. Can deliver DeliverSm PDU to the client.


## SmppGatewayDemo

Prototype for SMPP Gateway impelementaion. Shows how SubmitSm and DeliverSm can be properly forwarded.

## TestLocalPerformnce

Starts SMPP Client and Server in single process and submit a batch of SubmitSm PDUs. Estimates the performance of the library on the local machine.

## TestSmppServer

Starts SMPP server as worker service in [.NET Generic Host](https://docs.microsoft.com/en-us/dotnet/core/extensions/generic-host) application.
The average performance for SubmitSm and DeliverSm is written to the console when client disconnects.

## TestTelemetry

The example application shows how to configure the telemetry exporter with OpenTelemetry. It demonstrates the setup of an SMPP server and SMPP client as worker services, 
showcasing the process of sending a few concatenated messages. 
This example implementation offers developers a practical reference for leveraging the telemetry feature effectively in their own SMPP applications.

Requires Inetlab.SMPP v.2.9.28 or later and Target Framework .NET 6.0 or later

## UssdServiceDemo

Template application for implementing USSD service with SmppClient.

