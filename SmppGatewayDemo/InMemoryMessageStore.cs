﻿using System;
using System.Collections.Concurrent;
using Inetlab.SMPP.Common;

namespace SmppGatewayDemo
{
    public class InMemoryMessageStore : ISMSMessageStore
    {
        private readonly ConcurrentDictionary<string, SMSMessage> _store = new ConcurrentDictionary<string, SMSMessage>();
        private readonly ConcurrentDictionary<string, SMSMessage> _storeByRemoteMessageId = new ConcurrentDictionary<string, SMSMessage>();


        public void Add(SMSMessage message, EncodingMapper encodingMapper)
        {
            lock (_store)
            {
                if (!_store.TryAdd(message.Id, message))
                {
                    throw new InvalidOperationException($"{nameof(SMSMessage)} with the same Id '{message.Id}' already exists.");
                }

                if (!string.IsNullOrEmpty(message.RemoteMessageId))
                {

                    if (!_storeByRemoteMessageId.TryAdd(message.RemoteMessageId, message))
                    {
                        throw new InvalidOperationException(
                            $"{nameof(SMSMessage)} with the same remote MessageId '{message.RemoteMessageId}' already exists.");
                    }

                }
            }

        }

        public SMSMessage Update(string messageId, Action<SMSMessage> update, EncodingMapper encodingMapper)
        {
            lock (_store)
            {

                return _store.AddOrUpdate(messageId,
                    id => throw new InvalidOperationException($"SMS message with id {messageId} not found."),
                    (s, message) =>
                    {
                        update(message);

                        if (!string.IsNullOrEmpty(message.RemoteMessageId))
                        {
                            _storeByRemoteMessageId.TryAdd(message.RemoteMessageId, message);
                        }

                        return message;
                    });

            }
        }

        public bool TryUpdateByRemoteMessageId(string remoteMessageId, Action<SMSMessage> update,
            EncodingMapper clientEncodingMapper, out SMSMessage message)
        {
            lock (_store)
            {
                if (!_storeByRemoteMessageId.TryGetValue(remoteMessageId, out message)) return false;

                update(message);



                return true;
            }
        }

        public void Remove(string messageId)
        {
            SMSMessage oldMessage;

            lock (_store)
            {

                if (!_store.TryRemove(messageId, out oldMessage))
                {
                    throw new InvalidOperationException($"{nameof(SMSMessage)} with the Id '{messageId}' doesn't exist.");
                }

                if (!string.IsNullOrEmpty(oldMessage.RemoteMessageId))
                {
                    SMSMessage message;
                    if (!_storeByRemoteMessageId.TryRemove(oldMessage.RemoteMessageId, out message))
                    {
                        throw new InvalidOperationException($"{nameof(SMSMessage)} with the Id '{oldMessage.RemoteMessageId}' doesn't exist.");
                    }

                }
            }


        }




    }


}