﻿using System;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Inetlab.SMPP;
using Inetlab.SMPP.Common;
using Inetlab.SMPP.Logging;
using Inetlab.SMPP.PDU;

namespace SmppGatewayDemo
{
    public class Program
    {
        private SmppClient _proxyClient;
        private SmppServer _proxyServer;

        private readonly MessagesBridge _bridge;

        public static void Main(string[] args)
        {
            LogManager.SetLoggerFactory(new ConsoleLogFactory(LogLevel.Info));

            Console.Title = "Test Gateway";

            Uri url = null;
            if (args.Length > 0)
            {
                Uri.TryCreate(args[0], UriKind.Absolute, out url);
            }

            if (url == null)
            {
                url = new Uri("tcp://localhost:7777");
            }

            Program p = new Program();
            p.Run(url).ConfigureAwait(false);

            Console.ReadLine();
        }

        public Program()
        {
            ISMSMessageStore store = new InMemoryMessageStore();
            _bridge = new MessagesBridge(store);
            _bridge.ReceiptReadyForForward += WhenReceiptReadyForForward;
        }

        private async Task Run(Uri url)
        {


            _proxyServer = new SmppServer(new IPEndPoint(IPAddress.Any, 7776));
            _proxyServer.Name = "Proxy" + _proxyServer.Name;

            _proxyServer.evClientBind += (sender, client, data) => { };

            _proxyServer.evClientSubmitSm += (sender, serverClient, data) =>
            {
                Task.Run(() => ForwardSubmitSm(serverClient, data));
            };

            _proxyServer.evClientConnected += (sender, client) =>
            {

                client.ReceiveTaskScheduler = TaskScheduler.Default;


            };
            _proxyServer.evClientDisconnected += (sender, client) =>
            {

            };

            await _proxyServer.StartAsync();


            _proxyClient = new SmppClient();
            _proxyClient.ConnectionRecovery = true;
            _proxyClient.ConnectionRecoveryDelay = TimeSpan.FromSeconds(5);
            _proxyClient.ReceiveTaskScheduler = TaskScheduler.Default;
            _proxyClient.Name = "Proxy" + _proxyClient.Name;

            _proxyClient.evDeliverSm += (sender, data) =>
            {
                if (data.MessageType == MessageTypes.SMSCDeliveryReceipt)
                {
                    Task.Run(() => _bridge.ReceiptReceived(data))
                        .ContinueWith(t =>
                            {
                                _proxyClient.Logger.Error(t.Exception, "ReceiptReceived failed");
                            }
                            , TaskContinuationOptions.OnlyOnFaulted);
                }
                else
                {
                    Task.Run(() => ForwardDeliverSm(data));
                }

            };



            await _proxyClient.RetryUntilConnectedAsync(url.Host, url.Port, TimeSpan.FromSeconds(5));
            BindResp bindResp = await _proxyClient.BindAsync("proxy", "test");


        }

        private async Task ForwardSubmitSm(SmppServerClient serverClient, SubmitSm data)
        {
            try
            {
                string messageId = data.Response.MessageId;

                _bridge.SubmitReceived(messageId, data);

                SubmitSm submitSm = data.ClonePDU();

                submitSm.Header.Sequence = 0;

                SubmitSmResp resp = await _proxyClient.SubmitWithRepeatAsync(submitSm, TimeSpan.FromSeconds(5));

                if (resp.Header.Status == CommandStatus.ESME_ROK)
                {
                    _bridge.SubmitForwarded(messageId, resp);
                }

                if (data.SMSCReceipt == SMSCDeliveryReceipt.NotRequested)
                {
                    _bridge.DeliveryReceiptNotRequested(messageId);
                }
                else if (resp.Header.Status != CommandStatus.ESME_ROK)
                {
                    _ = SendUndeliverableReceiptAsync(serverClient, data);
                }



            }
            catch (Exception ex)
            {
                serverClient.Logger.Error(ex, "Failed to process SubmitSm.");
            }
        }

        private static async Task SendUndeliverableReceiptAsync(SmppServerClient serverClient, SubmitSm submitSm)
        {
            await serverClient.DeliverAsync(SMS.ForDeliver().From(submitSm.DestinationAddress).To(submitSm.SourceAddress).Receipt(
                    new Receipt
                    {
                        MessageId = submitSm.Response.MessageId,
                        DoneDate = DateTime.Now,
                        State = MessageState.Undeliverable
                    }
                    ));

        }


        private async Task WhenReceiptReadyForForward(string systemId, DeliverSm data)
        {
            SmppServerClient client = _proxyServer.ConnectedClients.FirstOrDefault(x => x.SystemID == systemId
                && (x.BindingMode == ConnectionMode.Receiver || x.BindingMode == ConnectionMode.Transceiver));
            if (client != null)
            {

                DeliverSmResp resp = await client.DeliverAsync(data);
                if (resp.Header.Status == CommandStatus.ESME_ROK)
                {
                    _bridge.ReceiptDelivered(data.Receipt.MessageId);
                }
                else
                {
                    _proxyServer.Logger.Warn($"Failed to deliver the Receipt: {resp}");
                    _bridge.FailedToDeliverReceipt(data.Receipt.MessageId);
                }

            }
            else
            {
                _proxyServer.Logger.Warn($"Unable to find the client '{systemId}' for the Receipt: {data}");
            }
        }

        private async Task ForwardDeliverSm(DeliverSm data)
        {
            try
            {
                var client = FindDestination(data.DestinationAddress);
                if (client != null)
                {
                    DeliverSmResp resp = await client.DeliverAsync(data);
                }
                else
                {
                    // save DeliverSm for delivery when client connects.
                }
            }
            catch (Exception ex)
            {
                _proxyClient.Logger.Error(ex, "Failed to forward DeliverSm.");
            }

        }

        private SmppServerClient FindDestination(SmeAddress address)
        {
            foreach (SmppServerClient client in _proxyServer.ConnectedClients)
            {
                if (client.BindingMode == ConnectionMode.Transmitter) continue;

                if (!string.IsNullOrEmpty(client.EsmeAddress?.Address))
                {
                    Regex regex = new Regex(client.EsmeAddress?.Address);
                    if (regex.IsMatch(address.Address))
                    {
                        return client;
                    }
                }

            }

            return null;
        }
    }
}
