﻿using System;
using Inetlab.SMPP.Common;

namespace SmppGatewayDemo
{
    public interface ISMSMessageStore
    {

        /// <summary>
        /// Adds new SMS message to the store
        /// </summary>
        /// <param name="message">The SMS message object.</param>
        /// <param name="encodingMapper">The <see cref="EncodingMapper"/> required to serialize PDU to byte array.</param>
        void Add(SMSMessage message, EncodingMapper encodingMapper);

        /// <summary>
        /// Gets the SMS message by local message id and updates its state.
        /// </summary>
        /// <param name="messageId">The local message id</param>
        /// <param name="update">The action to update the SMS message state. </param>
        /// <param name="encodingMapper">The <see cref="EncodingMapper"/> required to serialize PDU to byte array.</param>
        /// <returns></returns>
        SMSMessage Update(string messageId, Action<SMSMessage> update, EncodingMapper encodingMapper);

        /// <summary>
        /// Finds SMS message by remote MessageId and updates the state.
        /// </summary>
        /// <param name="remoteMessageId">Remote message id.</param>
        /// <param name="update">The action to update the SMS message state. </param>
        /// <param name="encodingMapper">The <see cref="EncodingMapper"/> required to serialize PDU to byte array.</param>
        /// <param name="message">The SMS message</param>
        bool TryUpdateByRemoteMessageId(string remoteMessageId, Action<SMSMessage> update,
            EncodingMapper encodingMapper, out SMSMessage message);

        /// <summary>
        /// Removes the SMS message from the store
        /// </summary>
        /// <param name="messageId">The local message id</param>
        void Remove(string messageId);


    }
}