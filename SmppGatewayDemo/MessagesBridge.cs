﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using Inetlab.SMPP.Parameters;
using Inetlab.SMPP.PDU;

namespace SmppGatewayDemo
{
    public delegate Task ReceiptReadyForForwardDelegate(string systemId, DeliverSm receipt);

    public class MessagesBridge
    {
        private readonly ISMSMessageStore _store;
        private readonly ConcurrentDictionary<string, DeliverSm> _receivedDeliverSm = new ConcurrentDictionary<string, DeliverSm>();

        public MessagesBridge(ISMSMessageStore store)
        {
            _store = store;
        }

        public event ReceiptReadyForForwardDelegate ReceiptReadyForForward;

        public void SubmitReceived(string messageId, SubmitSm data)
        {
            if (data.Client == null) throw new ArgumentNullException($"Client is not defined in {nameof(SubmitSm)}.");

            var message = new SMSMessage
            {
                Id = messageId,
                SequenceNumber = data.Header.Sequence,
                SystemID = data.Client.SystemID
            };


            _store.Add(message, data.Client.EncodingMapper);
        }


        public void SubmitForwarded(string messageId, SubmitSmResp remoteResp)
        {
            if (remoteResp.Client == null)
                throw new ArgumentNullException($"Client is not defined in {nameof(SubmitSmResp)}.");

            bool readyToForward = false;

            SMSMessage message = _store.Update(messageId, m =>
            {
                m.RemoteSequence = remoteResp.Header.Sequence;
                m.RemoteMessageId = remoteResp.MessageId;

                //Handle the case when SubmitSmResp received after DeliverSm
                DeliverSm receipt;
                if (_receivedDeliverSm.TryRemove(remoteResp.MessageId, out receipt))
                {
                    readyToForward = true;
                    m.Receipt = receipt;
                    m.DeliveryState = receipt.Receipt.State.ToString();
                }
            }, remoteResp.Client.EncodingMapper);


            if (readyToForward)
            {
                OnReceiptReadyForForward(message);
            }
        }

        public void DeliveryReceiptNotRequested(string localMessageId)
        {
            _store.Remove(localMessageId);
        }

        public void ReceiptReceived(DeliverSm deliverSm)
        {
            if (deliverSm.Client == null)
                throw new ArgumentNullException($"Client is not defined in {nameof(DeliverSm)}.");

            //Save received DeliverSm. We shall remove it when we find or receive SubmitSmResp.
            if (!_receivedDeliverSm.TryAdd(deliverSm.Receipt.MessageId, deliverSm))
            {
                throw new InvalidOperationException(
                    $"Cannot save delivery receipt for further processing. Another PDU with same message_id '{deliverSm.Receipt.MessageId}' already saved.");
            }

            SMSMessage message;

            if (_store.TryUpdateByRemoteMessageId(
                deliverSm.Receipt.MessageId,
                m =>
                {
                    m.Receipt = deliverSm;

                    m.DeliveryState = deliverSm.Receipt.State.ToString();
                },
                deliverSm.Client.EncodingMapper, out message))
            {
                //We have found SubmitSmResp, therefore we can remove saved DeliverSm.
                if (_receivedDeliverSm.TryRemove(message.RemoteMessageId, out _))
                {
                    OnReceiptReadyForForward(message);
                }
            }

        }

        public void ReceiptDelivered(string localMessageId)
        {
            _store.Remove(localMessageId);
        }

        public void FailedToDeliverReceipt(string localMessageId)
        {
            _store.Remove(localMessageId);
        }

        private void OnReceiptReadyForForward(SMSMessage message)
        {
            var handler = ReceiptReadyForForward;
            if (handler != null)
            {
                DeliverSm receipt = message.Receipt;
                receipt.Receipt.MessageId = message.Id;

                var parameter = receipt.Parameters.Of<ReceiptedMessageIdParameter>().FirstOrDefault();
                if (parameter != null)
                {
                    receipt.Parameters.Remove(parameter);
                    receipt.Parameters.Add(new ReceiptedMessageIdParameter(receipt.Receipt.MessageId));
                }

                receipt.Header.Sequence = 0;

                handler(message.SystemID, receipt);
            }
        }

    }
}
