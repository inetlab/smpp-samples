﻿using Inetlab.SMPP.PDU;

namespace SmppGatewayDemo
{
    /// <summary>
    /// Represents a SMS message. Long SMS text message can be represented as collection of <see cref="SMSMessage"/> instances.
    /// </summary>
    public class SMSMessage
    {
        /// <summary>
        /// Local message id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Sequence number of received SubmitSm from client
        /// </summary>
        public uint SequenceNumber { get; set; }

        /// <summary>
        /// Sequence number of SubmitSm sent to the SMSC.
        /// </summary>
        public uint RemoteSequence { get; set; }

        /// <summary>
        /// MessageId received from SMSC server in SubmitSmResp.
        /// </summary>
        public string RemoteMessageId { get; set; }

        /// <summary>
        /// Delivery receipt received from SMSC server.
        /// </summary>
        public DeliverSm Receipt { get; set; }

        /// <summary>
        /// Delivery state from the receipt
        /// </summary>
        public string DeliveryState { get; set; }

        /// <summary>
        /// Username of the client that sent SubmitSm PDU.
        /// </summary>
        public string SystemID { get; set; }
    }
}