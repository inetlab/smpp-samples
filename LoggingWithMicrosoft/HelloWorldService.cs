﻿using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Inetlab.SMPP;
using Inetlab.SMPP.Common;
using Inetlab.SMPP.Logging;
using Inetlab.SMPP.PDU;
using Microsoft.Extensions.Hosting;

namespace LoggingWithMicrosoft
{
    public class HelloWorldService : IHostedService
    {
        private readonly SmppServer _server;

        public HelloWorldService()
        {
            _server = new SmppServer(new IPEndPoint(IPAddress.Any, 7777));
            _server.evClientBind += ServerOnClientBind;
            _server.evClientSubmitSm += ServerOnClientSubmitSm;

        }

        private void ServerOnClientSubmitSm(object sender, SmppServerClient client, SubmitSm data)
        {
        }

        private void ServerOnClientBind(object sender, SmppServerClient client, Bind data)
        {
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await _server.StartAsync(cancellationToken);

            _ = Task.Run(Run);

        }

        private async Task Run()
        {
            using var client = new SmppClient();

            if (await client.ConnectAsync("localhost", 7777))
            {
                BindResp bindResp = await client.BindAsync("1", "2");

                if (bindResp.Header.Status == CommandStatus.ESME_ROK)
                {
                    var submitResp = await client.SubmitAsync(
                        SMS.ForSubmit()
                            .From("111")
                            .To("222")
                            .Coding(DataCodings.UCS2)
                            .Text("Hello World!"));

                    if (submitResp.All(x => x.Header.Status == CommandStatus.ESME_ROK))
                    {
                        client.Logger.Info("SMS MESSAGE HAS BEEN SENT!");
                    }
                }
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return _server.StopAsync();
        }
    }
}
