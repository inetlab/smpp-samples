﻿using Microsoft.Extensions.Hosting;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using LogLevel = Inetlab.SMPP.Logging.LogLevel;

namespace LoggingWithMicrosoft
{
    public class Program
    {
        public static Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            var loggerFactory = host.Services.GetRequiredService<ILoggerFactory>();

            //Enable debug logging for Inetlab.SMPP library
            Inetlab.SMPP.Logging.LogManager.SetLoggerFactory(new LoggerFactoryAdapter(loggerFactory, LogLevel.Debug));

            return host.RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseConsoleLifetime()
                .ConfigureLogging(logging =>
                {
                    logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Debug);
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<HelloWorldService>();
                });


    }
}
