﻿using System;
using Microsoft.Extensions.Logging;


namespace LoggingWithMicrosoft
{
    public class LoggerFactoryAdapter : Inetlab.SMPP.Logging.ILogFactory
    {
        private readonly ILoggerFactory _loggerFactory;
        private readonly Inetlab.SMPP.Logging.LogLevel _minLevel;

        public LoggerFactoryAdapter(ILoggerFactory loggerFactory) : this(loggerFactory, Inetlab.SMPP.Logging.LogLevel.Info)
        {

        }

        public LoggerFactoryAdapter(ILoggerFactory loggerFactory, Inetlab.SMPP.Logging.LogLevel minLevel)
        {
            _loggerFactory = loggerFactory;
            _minLevel = minLevel;
        }

        public Inetlab.SMPP.Logging.ILog GetLogger(string name)
        {
            return new LoggerFactoryLogger(_loggerFactory.CreateLogger(name), _minLevel);
        }


    }

    public class LoggerFactoryLogger : Inetlab.SMPP.Logging.ILog
    {
        private readonly ILogger _logger;
        private readonly Inetlab.SMPP.Logging.LogLevel _minLevel;


        public LoggerFactoryLogger(ILogger logger, Inetlab.SMPP.Logging.LogLevel minLevel)
        {
            _logger = logger;
            _minLevel = minLevel;
        }


        private static LogLevel GetLevel(Inetlab.SMPP.Logging.LogLevel level)
        {
            return level switch
            {
                Inetlab.SMPP.Logging.LogLevel.Verbose => LogLevel.Trace,
                Inetlab.SMPP.Logging.LogLevel.Debug => LogLevel.Debug,
                Inetlab.SMPP.Logging.LogLevel.Info => LogLevel.Information,
                Inetlab.SMPP.Logging.LogLevel.Warning => LogLevel.Warning,
                Inetlab.SMPP.Logging.LogLevel.Error => LogLevel.Error,
                Inetlab.SMPP.Logging.LogLevel.Fatal => LogLevel.Critical,
                Inetlab.SMPP.Logging.LogLevel.All => throw new NotImplementedException(),
                Inetlab.SMPP.Logging.LogLevel.Off => throw new NotImplementedException(),
                _ => LogLevel.None
            };
        }

        public bool IsEnabled(Inetlab.SMPP.Logging.LogLevel level)
        {
            return level >= _minLevel && _logger.IsEnabled(GetLevel(level));
        }

        public void Write(Inetlab.SMPP.Logging.LogLevel level, string message, Exception exception = null, params object[] values)
        {
            if (level < _minLevel) return;

#pragma warning disable CA1848 // Use the LoggerMessage delegates
            _logger.Log(GetLevel(level), exception, message, values);
#pragma warning restore CA1848 // Use the LoggerMessage delegates
        }

    }


}
