@echo off
rem setlocal enabledelayedexpansion

set Configuration=Release

for /f %%i in ("%0") do set ExecDir=%%~dpi


IF EXIST %ExecDir%SmppClientDemo\bin\%Configuration%\SmppClientDemo.exe GOTO START_DEMO

echo Before starting Demo applications you need to build Visual Studio Solution Samples.sln.
SET /P answer="Start building (Y/N)?"

IF /I "%answer%" EQU "N" GOTO END


:BUILD

set vswhere="%ProgramFiles(x86)%\Microsoft Visual Studio\Installer\vswhere.exe"

for /f "usebackq tokens=1* delims=: " %%i in (`%vswhere% -latest -requires Microsoft.Component.MSBuild`) do (
  if /i "%%i"=="installationPath" set InstallDir=%%j
)

IF "%InstallDir%" == "" (

  for /f "usebackq tokens=1* delims=: " %%i in (`%vswhere% -latest -products Microsoft.VisualStudio.Product.BuildTools`) do (
    if /i "%%i"=="installationPath" set InstallDir=%%j
  )
)


set MSBuild="%InstallDir%\MSBuild\15.0\Bin\MSBuild.exe"


%MSBuild% -t:restore  %ExecDir%Samples.sln 

%MSBuild% %ExecDir%Samples.sln /p:Configuration=%Configuration%

rem  dotnet restore %~DP0Samples.sln

rem  dotnet build %~DP0Samples.sln

:START_DEMO

IF EXIST %ExecDir%SmppClientDemo\bin\%Configuration%\SmppClientDemo.exe (
  start %ExecDir%SmppClientDemo\bin\%Configuration%\SmppClientDemo.exe
)
IF EXIST %ExecDir%SmppServerDemo\bin\%Configuration%\SmppServerDemo.exe (
  start %ExecDir%SmppServerDemo\bin\%Configuration%\SmppServerDemo.exe
)

:END