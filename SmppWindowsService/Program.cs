﻿using System;
using System.ServiceProcess;

namespace SmppWindowsService
{
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static void Main()
        {
            //Fix Current dir for windows service
            System.IO.Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);


            var servicesToRun = new ServiceBase[]
            {
                new SmppServerWindowsService(),
                new SmppClientWindowsService(),
            };

            ServiceBase.Run(servicesToRun);
        }
    }
}
