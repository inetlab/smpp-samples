# TestTelemetry

The example application shows how to configure the telemetry exporter with OpenTelemetry. It demonstrates the setup of an SMPP server and SMPP client as worker services, 
showcasing the process of sending a few concatenated messages. 
This example implementation offers developers a practical reference for leveraging the telemetry feature effectively in their own SMPP applications.

Requires Inetlab.SMPP v.2.9.28 or later and Target Framework .NET 6.0 or later

