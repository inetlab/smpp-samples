﻿using System.Diagnostics;

namespace TestTelemetry.Diagnostics
{
    internal static class ActivitySources
    {
        public static ActivitySource Client = new ActivitySource(nameof(TestSmppClientService));
        public static ActivitySource Server = new ActivitySource(nameof(TestSmppServerService));
    }
}
