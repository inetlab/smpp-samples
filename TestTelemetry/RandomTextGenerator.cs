﻿using System;
using System.Text;

namespace TestTelemetry
{
    public class RandomTextGenerator
    {
        private static readonly Random s_random = new Random();

        private static readonly string[] s_words =
        {
            "Lorem", "ipsum", "dolor", "sit", "amet", "consectetur",
            "adipiscing", "elit", "sed", "do", "eiusmod", "tempor",
            "incididunt", "ut", "labore", "et", "dolore", "magna",
            "aliqua", "Ut", "enim", "ad", "minim", "veniam",
            "quis", "nostrud", "exercitation", "ullamco", "laboris",
            "nisi", "ut", "aliquip", "ex", "ea", "commodo",
            "consequat", "Duis", "aute", "irure", "dolor", "in",
            "reprehenderit", "in", "voluptate", "velit", "esse", "cillum",
            "dolore", "eu", "fugiat", "nulla", "pariatur", "Excepteur",
            "sint", "occaecat", "cupidatat", "non", "proident", "sunt",
            "in", "culpa", "qui", "officia", "deserunt", "mollit",
            "anim", "id", "est", "laborum"
        };
        public static string GenerateRandomText()
        {
            int length = s_random.Next(200, 501); // Generate random length between 20 and 500
            StringBuilder sb = new StringBuilder(length);

            while (sb.Length < length)
            {
                string randomWord = s_words[s_random.Next(s_words.Length)];
                sb.Append(randomWord).Append(' ');
            }

            return sb.ToString().Trim();
        }
    }
}
