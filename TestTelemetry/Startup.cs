﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;

namespace TestTelemetry
{
    internal class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        public void ConfigureServices(IServiceCollection services)
        {
            // Configure spans exporter to OLTP collector.
            services.AddOpenTelemetry()
                .WithTracing(b =>
                {
                    b.ConfigureResource(r => r.AddService(typeof(Startup).Namespace, "1.0"));
                    b.AddSource("Inetlab.SMPP", nameof(TestSmppClientService), nameof(TestSmppServerService));
                    b.AddOtlpExporter();
                });

            // Add test SMPP server that receives messages from test SMPP client
            services.AddHostedService<TestSmppServerService>();

            // Add test SMPP client that sends few concatenated messages on start.
            services.AddHostedService<TestSmppClientService>();

        }
    }
}
