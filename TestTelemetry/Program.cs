﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace TestTelemetry
{
    public class Program
    {

        public static async Task<int> Main(string[] args)
        {
            Directory.SetCurrentDirectory(AppContext.BaseDirectory);


            IHostBuilder hostBuilder = CreateHostBuilder(args)
                .UseConsoleLifetime();


            using (IHost host = hostBuilder.Build())
            {

                var logger = host.Services.GetRequiredService<ILogger<Program>>();

                void OnUnobservedException(object sender, UnobservedTaskExceptionEventArgs eventArgs)
                {
                    logger.LogCritical(eventArgs.Exception, "Unobserved Task Exception.");
                }

                try
                {
                    TaskScheduler.UnobservedTaskException += OnUnobservedException;

                    await host.RunAsync();

                    return 0;
                }
                catch (ArgumentException ex)
                {
                    logger.LogError(ex.Message);
                }
                catch (Exception ex)
                {
                    logger.LogCritical(ex, "Application terminated unexpectedly.");
                }

            }

            return 1;
        }


        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args).ConfigureServices((context, services) =>
            {
                Startup startup = new Startup(context.Configuration);

                startup.ConfigureServices(services);
            });

    }

}




