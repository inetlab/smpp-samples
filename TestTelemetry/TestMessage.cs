﻿using System;

namespace TestTelemetry
{
    public class TestMessage
    {
        public Guid Id { get; }
        public string Text { get; }
        public string Source { get; }

        public string Destination { get; }

        public TestMessage(Guid id, string text, string source, string destination)
        {
            Id = id;
            Text = text;
            Source = source;
            Destination = destination;
        }
    }
}
