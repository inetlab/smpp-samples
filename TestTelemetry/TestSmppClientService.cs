﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Inetlab.SMPP;
using Inetlab.SMPP.Common;
using Inetlab.SMPP.PDU;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using TestTelemetry.Diagnostics;

namespace TestTelemetry
{
    internal class TestSmppClientService : IHostedService
    {
        private readonly ILogger<TestSmppClientService> _logger;
        private SmppClient _client;

        public TestSmppClientService(ILogger<TestSmppClientService> logger)
        {
            _logger = logger;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _client = new SmppClient();
            _client.SendQueueLimit = 1000; // limit window size
            _client.ResponseTimeout = TimeSpan.FromSeconds(2); // simulated timeout from test SMPP server
            _client.ConnectionRecovery = true;
            _client.ConnectionRecoveryDelay = TimeSpan.FromSeconds(5);
            _client.EnquireLinkInterval = TimeSpan.FromSeconds(60);

            SubscribeToEvents(_client);


            await EstablishSmppSessionAsync("localhost", 7777, cancellationToken);
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            if (_client == null) return;
            await _client.DisconnectAsync();
            _client.Dispose();
        }


        private async Task EstablishSmppSessionAsync(string host, int port, CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                string systemId = "test";
                string password = "test";

                var retryDelay = TimeSpan.FromSeconds(5);

                await _client.RetryUntilConnectedAsync(host, port, retryDelay, int.MaxValue, cancellationToken);
                _logger.LogInformation($"SmppClient has been connected to {host}:{port}");

                BindResp resp = await _client.BindAsync(systemId, password);

                if (resp.Header.Status == CommandStatus.ESME_ROK)
                {
                    _logger.LogInformation($"SmppClient has been bound as '{systemId}' to {resp.SystemId}");

                    OnConnected();
                    return;
                }

                _logger.LogWarning($"SmppClient cannot bind as {systemId}. Response status: {resp.Header.Status}");

                await _client.DisconnectAsync();

                await Task.Delay(retryDelay, cancellationToken);
            }

        }

        private void SubscribeToEvents(SmppClient client)
        {
            client.evServerCertificateValidation += (sender, args) =>
            {
                //accept all server certificates
                args.Accepted = true;
            };

            client.evDeliverSm += OnDeliverSm;
            client.evRecoverySucceeded += (_, _) => OnConnected();

        }

        private void OnConnected()
        {
            Task.Run(() => SendBatch(2));
        }

        private void SendBatch(int messagesCount)
        {
            int index = 0;

            foreach (var message in GetInfiniteMessages())
            {
                index++;
                //Set SendQueueLimit property to block the loop with the SubmitAsync method and prevent OutOfMemoryException
                _ = SubmitMessage(message);

                if (index == messagesCount) return;
            }

        }

        private async Task SubmitMessage(TestMessage message)
        {
            if (_client == null) throw new ArgumentNullException(nameof(_client));

            using (var activity = ActivitySources.Client.StartActivity("Submit SMS message"))
            {
                try
                {
                    activity?.AddTag("MessageId", message.Id);
                    activity?.AddTag("MessageText", message.Text);

                    var builder = SMS.ForSubmit().From(message.Source).To(message.Destination).Text(message.Text)
                        .DeliveryReceipt();

                    _logger.LogInformation($"Send message id: {message.Id}, TraceId: {activity?.TraceId}");

                    SubmitSmResp[] responses = await _client.SubmitAsync(builder).ConfigureAwait(false);

                    if (responses.All(r => r.Header.Status == CommandStatus.ESME_ROK))
                    {
                        activity?.SetStatus(ActivityStatusCode.Ok);

                        activity?.AddEvent(new ActivityEvent("save response to database"));
                        await Task.Delay(100); //simulate db code
                    }
                    else
                    {
                        activity?.SetStatus(ActivityStatusCode.Error);
                        foreach (SubmitSmResp response in responses)
                        {
                            activity?.AddEvent(
                                new ActivityEvent(
                                    $"sequence:{response.Header.Sequence} = {response.Header.Status}"));
                        }

                        string wrongStatus = string.Join(", ", responses.Select(x => x.Header.Status.ToString()));
                        _logger.LogError($"Wrong SubmitSmResp status: {wrongStatus}");
                    }
                }
                catch (Exception ex)
                {
                    activity?.SetStatus(ActivityStatusCode.Error, ex.Message);

                    _logger.LogError(ex, $"Failed to send message {message.Id}");
                }
            }
        }


        private static IEnumerable<TestMessage> GetInfiniteMessages(CancellationToken cancelToken = default)
        {
            int i = 0;
            while (!cancelToken.IsCancellationRequested)
            {
                i++;

                yield return new TestMessage(Guid.NewGuid(), RandomTextGenerator.GenerateRandomText(), "test", (100000 + i).ToString());

            }
        }




        private async void OnDeliverSm(object sender, DeliverSm data)
        {
            using (var activity = ActivitySources.Client.StartActivity("Process Delivery Receipt"))
            {
                await Task.Delay(100);
                activity?.SetStatus(ActivityStatusCode.Ok);
            }
        }


    }
}
