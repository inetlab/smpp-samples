﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Inetlab.SMPP;
using Inetlab.SMPP.Common;
using Inetlab.SMPP.PDU;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace TestTelemetry
{
    internal class TestSmppServerService : IHostedService
    {
        private readonly ILogger<TestSmppServerService> _logger;
        private SmppServer _server;

        public TestSmppServerService(ILogger<TestSmppServerService> logger)
        {
            _logger = logger;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _server = new SmppServer(new IPEndPoint(IPAddress.Any, 7777));

            SubscribeToEvents(_server);

            await _server.StartAsync(cancellationToken);
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            if (_server == null) throw new InvalidOperationException($"SmppServer was not started.");

            await _server.StopAsync();

            _server.Dispose();

            _server = null;
        }

        private void SubscribeToEvents(SmppServer server)
        {
            server.evClientConnected += OnClientConnected;
            server.evClientDisconnected += OnClientDisconnected;

            server.evClientBind += OnClientBind;
            server.evClientUnBind += OnClientUnbind;

            server.evClientSubmitSm += OnClientSubmitSm;

        }

        private void OnClientConnected(object sender, SmppServerClient client)
        {

        }

        private void OnClientDisconnected(object sender, SmppServerClient client)
        {

        }

        private void OnClientBind(object sender, SmppServerClient client, Bind data)
        {
            data.Response.Header.Status = CommandStatus.ESME_ROK;
        }

        private void OnClientUnbind(object sender, SmppServerClient client, UnBind data)
        {

        }

        private async void OnClientSubmitSm(object sender, SmppServerClient client, SubmitSm submitSm)
        {
            MessageState state = MessageState.Delivered;

            if (submitSm.Concatenation != null)
            {
                if (submitSm.Concatenation.SequenceNumber == 1)
                {
                    // simulate failed processing
                    submitSm.Response.Header.Status = CommandStatus.ESME_RSUBMITFAIL;
                    return;
                }

                if (submitSm.Concatenation.SequenceNumber == 2)
                {
                    // simulate response timeout on the client.
                    submitSm.Response = null;
                    return;
                }

                if (submitSm.Concatenation.SequenceNumber == 3)
                {
                    state = MessageState.Rejected;
                    // simulate rejected DLR.
                }
            }

            if (submitSm.RegisteredDelivery == 1)
            {

                try
                {
                    //Send Delivery Receipt when required
                    var resp = await client.DeliverAsync(
                        SMS.ForDeliver()
                            .To(submitSm.SourceAddress)
                            .From(submitSm.DestinationAddress)
                            .Coding(submitSm.DataCoding)
                            .Receipt(new Receipt
                            {
                                DoneDate = DateTime.Now,
                                State = state,
                                MessageId = submitSm.Response.MessageId,
                                ErrorCode = "0",
                                SubmitDate = DateTime.Now
                            }
                            )
                    );

                    foreach (DeliverSmResp deliverSmResp in resp)
                    {
                        if (deliverSmResp.Header.Status != CommandStatus.ESME_ROK)
                        {
                            _logger.LogError($"Wrong DeliverSmResp status. {deliverSmResp}");
                        }
                    }

                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"Failed to send delivery receipt for {submitSm}.");
                }

            }

        }
    }
}

