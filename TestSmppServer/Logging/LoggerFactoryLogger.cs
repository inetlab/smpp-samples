﻿using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.Logging;

namespace TestSmppServer.Logging
{
    [SuppressMessage("Performance", "CA1848:Use the LoggerMessage delegates")]
    public class LoggerFactoryLogger : Inetlab.SMPP.Logging.ILog
    {
        private readonly ILogger _logger;
        private readonly Inetlab.SMPP.Logging.LogLevel _minLevel;


        public LoggerFactoryLogger(ILogger logger, Inetlab.SMPP.Logging.LogLevel minLevel)
        {
            _logger = logger;
            _minLevel = minLevel;
        }


        private static LogLevel GetLevel(Inetlab.SMPP.Logging.LogLevel level)
        {
            return level switch
            {
                Inetlab.SMPP.Logging.LogLevel.All => LogLevel.Trace,
                Inetlab.SMPP.Logging.LogLevel.Verbose => LogLevel.Trace,
                Inetlab.SMPP.Logging.LogLevel.Debug => LogLevel.Debug,
                Inetlab.SMPP.Logging.LogLevel.Info => LogLevel.Information,
                Inetlab.SMPP.Logging.LogLevel.Warning => LogLevel.Warning,
                Inetlab.SMPP.Logging.LogLevel.Error => LogLevel.Error,
                Inetlab.SMPP.Logging.LogLevel.Fatal => LogLevel.Critical,
                Inetlab.SMPP.Logging.LogLevel.Off => LogLevel.None,
                _ => LogLevel.None,
            };
        }

        public bool IsEnabled(Inetlab.SMPP.Logging.LogLevel level)
        {
            return level >= _minLevel && _logger.IsEnabled(GetLevel(level));
        }

        public void Write(Inetlab.SMPP.Logging.LogLevel level, string message, Exception exception = null, params object[] values)
        {
            if (level < _minLevel) return;

            _logger.Log(GetLevel(level), exception, message, values);
        }

    }

}
