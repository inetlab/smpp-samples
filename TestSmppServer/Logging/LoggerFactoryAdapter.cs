﻿using Microsoft.Extensions.Logging;

namespace TestSmppServer.Logging
{
    public class LoggerFactoryAdapter : Inetlab.SMPP.Logging.ILogFactory
    {
        private readonly ILoggerFactory _loggerFactory;
        private readonly Inetlab.SMPP.Logging.LogLevel _minLevel;

        public LoggerFactoryAdapter(ILoggerFactory loggerFactory) : this(loggerFactory, Inetlab.SMPP.Logging.LogLevel.Info)
        {

        }

        public LoggerFactoryAdapter(ILoggerFactory loggerFactory, Inetlab.SMPP.Logging.LogLevel minLevel)
        {
            _loggerFactory = loggerFactory;
            _minLevel = minLevel;
        }

        public Inetlab.SMPP.Logging.ILog GetLogger(string name)
        {
            return new LoggerFactoryLogger(_loggerFactory.CreateLogger(name), _minLevel);
        }


    }
}