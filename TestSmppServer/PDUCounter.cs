﻿using System.Diagnostics;
using System.Threading;

namespace TestSmppServer
{
    public class PDUCounter
    {
        private long _total;

        private long _firstRowTicks;
        private long _lastRawTicks;

        public double MessagesPerSecond
        {
            get
            {
                if (_lastRawTicks == _firstRowTicks) return double.NaN;

                return Interlocked.Read(ref _total) / ((double)(_lastRawTicks - _firstRowTicks) / Stopwatch.Frequency);
            }
        }

        public long Total => Interlocked.Read(ref _total);

        public void Increase()
        {
            long rawTicks = Stopwatch.GetTimestamp();

            if (_firstRowTicks == 0) _firstRowTicks = rawTicks;
            _lastRawTicks = rawTicks;

            Interlocked.Increment(ref _total);


        }



    }
}