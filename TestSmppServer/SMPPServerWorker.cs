using System;
using System.Collections.Concurrent;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Inetlab.SMPP;
using Inetlab.SMPP.Common;
using Inetlab.SMPP.PDU;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace TestSmppServer
{
    [SuppressMessage("Performance", "CA1848:Use the LoggerMessage delegates")]
    public class SMPPServerWorker : IHostedService
    {
        private readonly ILogger<SMPPServerWorker> _logger;
        private SmppServer _server;

        private readonly ConcurrentDictionary<EndPoint, PDUCounter> _submitCounters = new ConcurrentDictionary<EndPoint, PDUCounter>();
        private readonly ConcurrentDictionary<EndPoint, PDUCounter> _deliverCounters = new ConcurrentDictionary<EndPoint, PDUCounter>();
        private readonly int? _receiveBufferSize;
        private readonly int? _sendBufferSize;
        public SMPPServerWorker(ILogger<SMPPServerWorker> logger)
        {
            _logger = logger;

            if (int.TryParse(Environment.GetEnvironmentVariable("RECEIVE_BUFFER_SIZE"), out int receiveBufferSize))
            {
                _receiveBufferSize = receiveBufferSize;
            }

            if (int.TryParse(Environment.GetEnvironmentVariable("SEND_BUFFER_SIZE"), out int sendBufferSize))
            {
                _sendBufferSize = sendBufferSize;
            }
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _server = new SmppServer(new IPEndPoint(IPAddress.Any, 7777));


            SubscribeToEvents(_server);

            await _server.StartAsync(cancellationToken);

        }



        public async Task StopAsync(CancellationToken cancellationToken)
        {
            if (_server == null) throw new InvalidOperationException($"SmppServer was not started.");

            await _server.StopAsync();

            _server.Dispose();

            _server = null;
        }


        private void SubscribeToEvents(SmppServer server)
        {
            server.evClientConnected += OnClientConnected;
            server.evClientDisconnected += OnClientDisconnected;

            server.evClientBind += OnClientBind;
            server.evClientUnBind += OnClientUnbind;

            server.evClientSubmitSm += OnClientSubmitSm;

        }

        private void OnClientConnected(object sender, SmppServerClient client)
        {
            client.ReceiveBufferSize = _receiveBufferSize;
            client.SendBufferSize = _sendBufferSize;

            client.ReceiveTaskScheduler = TaskScheduler.Default;

            PDUCounter submitCounter = new PDUCounter();
            _submitCounters.TryAdd(client.RemoteEndPoint, submitCounter);

            PDUCounter deliverCounter = new PDUCounter();
            _deliverCounters.TryAdd(client.RemoteEndPoint, deliverCounter);


        }

        private void OnClientDisconnected(object sender, SmppServerClient client)
        {

            PDUCounter counter;
            if (_submitCounters.TryRemove(client.RemoteEndPoint, out counter))
            {
                _logger.LogInformation($"Client: {client.RemoteEndPoint}, SubmitSm: Total: {counter.Total}, {counter.MessagesPerSecond} mps.");
            }

            if (_deliverCounters.TryRemove(client.RemoteEndPoint, out counter))
            {
                _logger.LogInformation($"Client: {client.RemoteEndPoint}, DeliverSm: Total: {counter.Total}, {counter.MessagesPerSecond} mps.");
            }
        }

        private void OnClientBind(object sender, SmppServerClient client, Bind bind)
        {

        }


        private void OnClientUnbind(object sender, SmppServerClient client, UnBind unbind)
        {


        }

        private void OnClientSubmitSm(object sender, SmppServerClient client, SubmitSm submitSm)
        {
            if (_submitCounters.TryGetValue(client.RemoteEndPoint, out PDUCounter counter))
            {
                counter.Increase();
            }

            if (submitSm.RegisteredDelivery == 1)
            {

                Task.Run(async () =>
                {
                    try
                    {
                        //Send Delivery Receipt when required
                        var resp = await client.DeliverAsync(
                            SMS.ForDeliver()
                                .To(submitSm.SourceAddress)
                                .From(submitSm.DestinationAddress)
                                .Coding(submitSm.DataCoding)
                                .Receipt(new Receipt
                                {
                                    DoneDate = DateTime.Now,
                                    State = MessageState.Delivered,
                                    MessageId = submitSm.Response.MessageId,
                                    ErrorCode = "0",
                                    SubmitDate = DateTime.Now
                                }
                                )
                        );

                        foreach (DeliverSmResp deliverSmResp in resp)
                        {
                            if (deliverSmResp.Header.Status != CommandStatus.ESME_ROK)
                            {
                                _logger.LogError($"Wrong DeliverSmResp status. {deliverSmResp}");
                            }
                        }

                        if (_deliverCounters.TryGetValue(client.RemoteEndPoint, out PDUCounter deliverCounter))
                        {
                            deliverCounter.Increase();
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, $"Failed to send delivery receipt for {submitSm}.");
                    }
                });
            }

        }


    }


}
