# TestSmppServer

Starts an SMPP server as a worker service within a [.NET Generic Host](https://docs.microsoft.com/en-us/dotnet/core/extensions/generic-host) Host application. 
The average performance for SubmitSm and DeliverSm is logged to the console when the client disconnects.
