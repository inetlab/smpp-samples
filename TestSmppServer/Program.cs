﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using TestSmppServer.Logging;
using LogLevel = Inetlab.SMPP.Logging.LogLevel;

namespace TestSmppServer
{
    [SuppressMessage("Performance", "CA1848:Use the LoggerMessage delegates")]
    public class Program
    {

        public static async Task<int> Main(string[] args)
        {
            Directory.SetCurrentDirectory(AppContext.BaseDirectory);


            IHostBuilder hostBuilder = CreateHostBuilder(args)
                .UseConsoleLifetime();


            using (IHost host = hostBuilder.Build())
            {

                Inetlab.SMPP.Logging.LogManager.SetLoggerFactory(new LoggerFactoryAdapter(host.Services.GetRequiredService<ILoggerFactory>(), LogLevel.Info));

                var logger = host.Services.GetRequiredService<ILogger<Program>>();

                try
                {

                    TaskScheduler.UnobservedTaskException += (sender, eventArgs) => { logger.LogCritical(eventArgs.Exception, "Unobserved Task Exception."); };
                    await host.RunAsync();

                    return 0;
                }
                catch (ArgumentException ex)
                {
                    logger.LogError(ex.Message);
                }
                catch (Exception ex)
                {
                    logger.LogCritical(ex, "Application terminated unexpectedly.");
                }

            }

            return 1;
        }


        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args).ConfigureServices((context, services) =>
            {
                Startup startup = new Startup(context.Configuration);

                startup.ConfigureServices(services);
            });

    }

}




