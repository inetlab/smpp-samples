﻿using Inetlab.SMPP.Parameters;

namespace UssdServiceDemo.Parameters
{
    public class UssdServiceOpParameter : TLV
    {
        public USSDOperation Operation { get; }
        public UssdServiceOpParameter(USSDOperation operation)
        {
            TagValue = OptionalTags.UssdServiceOp;
            Operation = operation;
            Value = new byte[] { (byte)operation };
        }

        public UssdServiceOpParameter(byte[] data)
        {
            TagValue = OptionalTags.UssdServiceOp;
            Value = data;

            Operation = (USSDOperation)data[0];
        }
    }

    public enum USSDOperation
    {
        PSSDIndication = 0,

        /// <summary> The Process Supplementary Service request (PSSR) from Mobile User to the application. </summary>
        PSSRIndication = 1,

        /// <summary> Unstructured Supplementary Service Request. Request from service and continue dialog.</summary>
        USSRRequest = 2,

        /// <summary> Unstructured Supplementary Service Notify. Notification from service without dialog.  </summary>
        /// <para>
        /// The Notify message differs from the Request message in that an Mobile User responds to a Notify message automatically.
        /// </para>
        USSNRequest = 3,

        PSSDResponse = 16,

        /// <summary> The response to Process Supplementary Service request (PSSRIndication). The reply from service. Dialog/Session ends. </summary>
        PSSRResponse = 17,

        /// <summary> Reply from MS.</summary>
        USSRConfirm = 18,

        USSNConfirm = 19,

    }

    //Defined by Huawei
    public static class HuaweiUSSDOperation
    {

        /// <summary> Session released by SP side when SP initiated USSD session </summary>
        public static USSDOperation SessionReleaseBySP = (USSDOperation)32;

        /// <summary> Abort for exception </summary>
        public static USSDOperation Abort = (USSDOperation)33;
    }
}