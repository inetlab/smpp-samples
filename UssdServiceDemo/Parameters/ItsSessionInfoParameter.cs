﻿using Inetlab.SMPP.Parameters;

namespace UssdServiceDemo.Parameters
{
    /// <summary>
    /// Represents its_session_info parameter with session number, sequence number of the dialogue unit and end of session indicator
    /// </summary>
    public class ItsSessionInfoParameter : TLV
    {
        /// <summary>
        /// The session number. Remains constant for each session.
        /// </summary>
        public byte Session { get; }

        /// <summary>
        /// The sequence number of the dialogue unit
        /// </summary>
        public byte Sequence { get; }

        /// <summary>
        /// Indicates the message is the end of the conversation session
        /// </summary>
        public bool EndOfSession { get; }

        public ItsSessionInfoParameter(byte session, byte sequence, bool endOfSession)
        {
            Session = session;
            Sequence = sequence;
            EndOfSession = endOfSession;

            TagValue = OptionalTags.ItsSessionInfo;

            Value = new byte[2];
            Value[0] = session;

            Value[1] = (byte)(sequence << 1);
            if (endOfSession)
                Value[1] = (byte)(Value[1] | 1);
        }

        public ItsSessionInfoParameter(byte[] data)
        {
            TagValue = OptionalTags.ItsSessionInfo;
            Value = data;

            Session = Value[0];
            Sequence = (byte)(Value[1] >> 1);
            EndOfSession = (byte)(Value[1] & 0x01) == 1;
        }
    }
}