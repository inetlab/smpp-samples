﻿using System;
using Inetlab.SMPP.Parameters;

namespace UssdServiceDemo.Parameters
{
    /// <summary>
    /// Represents its_session_info parameter only with session number and end of session indicator
    /// </summary>
    public class ItsSessionInfoParameter2 : TLV
    {
        /// <summary>
        /// The session number. Remains constant for each session.
        /// </summary>
        public int Session { get; }

        /// <summary>
        /// Indicates the message is the end of the conversation session
        /// </summary>
        public bool EndOfSession { get; }

        public ItsSessionInfoParameter2(int session, bool endOfSession)
        {
            Session = session;
            EndOfSession = endOfSession;

            TagValue = OptionalTags.ItsSessionInfo;

            Value = new byte[2];

            byte[] data = BitConverter.GetBytes(session);

            Value[1] = (byte)(data[0] << 1);
            if (endOfSession)
                Value[1] = (byte)(Value[1] | 1);
        }

        public ItsSessionInfoParameter2(byte[] data)
        {
            TagValue = OptionalTags.ItsSessionInfo;
            Value = data;

            Session = BitConverter.ToUInt16(new[] { data[0], (byte)(data[1] >> 1) }, 0);
            EndOfSession = (byte)(Value[1] & 1) == 1;
        }
    }
}