﻿using System.Threading.Tasks;
using Inetlab.SMPP.Common;
using Inetlab.SMPP.PDU;

namespace UssdServiceDemo
{
    public interface IUssdPort
    {
        bool IsDialogStarted(DeliverSm data);

        bool IsDialogFinished(DeliverSm data);

        bool IsReply(DeliverSm data);

        Task<SubmitSmResp[]> SendReplyAsync(DeliverSm data, string responseText, bool endDialog);

        Task<SubmitSmResp[]> StartDialogAsync(SmeAddress recepient, string text);

        Task<SubmitSmResp[]> NotifyAsync(SmeAddress recepient, string text);

    }
}