﻿using System;
using System.Threading.Tasks;
using Inetlab.SMPP;
using Inetlab.SMPP.Common;
using Inetlab.SMPP.Logging;
using Inetlab.SMPP.PDU;
using UssdServiceDemo.Adapters;

namespace UssdServiceDemo
{
    public class Program
    {
        private readonly SmppClient _client;
        private readonly IUssdPort _ussdPort;

        public static async Task Main(string[] args)
        {
            LogManager.SetLoggerFactory(new ConsoleLogFactory(LogLevel.Info));

            Console.Title = "USSD Service Demo";


            Program p = new Program();
            await p.RunAsync();
            Console.ReadLine();
        }

        public Program()
        {
            _client = new SmppClient();
            _client.ConnectionRecovery = true;

            _ussdPort = new EastWindUSSDAdapter(_client, new SmeAddress("USSDSERVICE"));

            _client.evDeliverSm += (sender, data) =>
            {
                if (data.MessageType == MessageTypes.Default)
                {
                    OnMessageReceived(data);
                }
            };


        }



        public async Task RunAsync()
        {
            await _client.RetryUntilConnectedAsync("localhost", 7777, TimeSpan.FromSeconds(5));

            BindResp bindResp = await _client.BindAsync("username", "password", ConnectionMode.Transceiver);
            if (bindResp.Header.Status == CommandStatus.ESME_ROK)
            {

            }

        }

        private void OnMessageReceived(DeliverSm data)
        {

            if (_ussdPort.IsDialogStarted(data))
            {
                //Mobile user initiates a dialog

                string requestText = data.GetMessageText(_client.EncodingMapper);
                _ = _ussdPort.SendReplyAsync(data, "You have sent: " + requestText, endDialog: false);

            }
            else if (_ussdPort.IsReply(data))
            {

                //Continue message
                string requestText = data.GetMessageText(_client.EncodingMapper);
                _ = _ussdPort.SendReplyAsync(data, "You have replied: " + requestText, endDialog: true);

            }
            else if (!_ussdPort.IsDialogFinished(data))
            {
                // clear dialog state
            }

        }

    }


}
