﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Inetlab.SMPP;
using Inetlab.SMPP.Common;
using Inetlab.SMPP.Parameters;
using Inetlab.SMPP.PDU;
using UssdServiceDemo.Parameters;

namespace UssdServiceDemo.Adapters
{
    public class TelcoUssdAdapter : IUssdPort
    {
        private readonly SmppClient _client;
        private readonly SmeAddress _serviceAddress;
        private readonly string _serviceType;

        public TelcoUssdAdapter(SmppClient client, SmeAddress serviceAddress, string serviceType)
        {
            _client = client;
            _serviceAddress = serviceAddress;
            _serviceType = serviceType;

            TLVCollection.RegisterParameter<UssdServiceOpParameter>(Convert.ToUInt16(0x0501));
            TLVCollection.RegisterParameter<ItsSessionInfoParameter>(Convert.ToUInt16(0x1383));
        }
        public Task<SubmitSmResp[]> SendReplyAsync(DeliverSm data, string responseText, bool endDialog)
        {
            var responseBuilder = SMS.ForSubmit()
                .ServiceType(_serviceType)
                .From(_serviceAddress)
                .To(data.SourceAddress)
                .Text(responseText);

            //responseBuilder.Coding((DataCodings) 0x15);

            if (!endDialog)
            {
                responseBuilder.AddParameter(new UssdServiceOpParameter(USSDOperation.USSRRequest));
            }

            var sessionInfo = data.Parameters.Of<ItsSessionInfoParameter>().FirstOrDefault();

            if (sessionInfo != null)
            {
                responseBuilder.AddParameter(new ItsSessionInfoParameter(sessionInfo.Session, sessionInfo.Sequence, endDialog));
            }
            else
            {
                responseBuilder.AddParameter(new ItsSessionInfoParameter(0, 0, endDialog));
            }

            return _client.SubmitAsync(responseBuilder);
        }

        public bool IsDialogStarted(DeliverSm data)
        {
            var opParameter = data.Parameters.Of<UssdServiceOpParameter>().FirstOrDefault();
            if (opParameter != null)
            {
                var sessionInfo = data.Parameters.Of<ItsSessionInfoParameter>().FirstOrDefault();
                if (sessionInfo != null)
                {
                    return opParameter.Operation == USSDOperation.PSSRIndication && !sessionInfo.EndOfSession;
                }
            }

            return false;
        }

        public bool IsDialogFinished(DeliverSm data)
        {
            var sessionInfo = data.Parameters.Of<ItsSessionInfoParameter>().FirstOrDefault();
            if (sessionInfo != null)
            {
                return sessionInfo.EndOfSession;
            }

            return false;
        }

        public bool IsReply(DeliverSm data)
        {
            var opParameter = data.Parameters.Of<UssdServiceOpParameter>().FirstOrDefault();
            if (opParameter != null)
            {
                var sessionInfo = data.Parameters.Of<ItsSessionInfoParameter>().FirstOrDefault();
                if (sessionInfo != null)
                {
                    return opParameter.Operation == USSDOperation.USSRConfirm && !sessionInfo.EndOfSession;
                }
            }

            return false;
        }

        public Task<SubmitSmResp[]> StartDialogAsync(SmeAddress recepient, string text)
        {
            throw new NotImplementedException();
        }

        public Task<SubmitSmResp[]> NotifyAsync(SmeAddress recepient, string text)
        {
            throw new NotImplementedException();
        }
    }
}
