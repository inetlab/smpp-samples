﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Inetlab.SMPP;
using Inetlab.SMPP.Common;
using Inetlab.SMPP.Parameters;
using Inetlab.SMPP.PDU;
using UssdServiceDemo.Parameters;

namespace UssdServiceDemo.Adapters
{
    public class EastWindUSSDAdapter : IUssdPort
    {
        private readonly SmppClient _client;
        private readonly SmeAddress _serviceAddress;
        private readonly string _serviceType = "USSD";

        public EastWindUSSDAdapter(SmppClient client, SmeAddress serviceAddress)
        {
            _client = client;
            _serviceAddress = serviceAddress;

            TLVCollection.RegisterParameter<UssdServiceOpParameter>(Convert.ToUInt16(0x0501));
        }

        public Task<SubmitSmResp[]> SendReplyAsync(DeliverSm data, string responseText, bool endDialog)
        {

            var responseBuilder = SMS.ForSubmit()
                .ServiceType(_serviceType)
                .From(_serviceAddress)
                .To(data.SourceAddress)
                .Text(responseText);

            //responseBuilder.Coding((DataCodings) 0x15);

            USSDOperation ussdOperation = (USSDOperation)34;
            if (endDialog)
            {
                ussdOperation = USSDOperation.PSSRResponse;
            }

            responseBuilder.AddParameter(new UssdServiceOpParameter(ussdOperation));


            //var sessionInfo = data.Parameters.Of<ItsSessionInfoParameter>().FirstOrDefault();

            //if (sessionInfo != null)
            //{
            //    responseBuilder.AddParameter(new ItsSessionInfoParameter(sessionInfo.Session, sessionInfo.Sequence, endDialog));
            //}

            return _client.SubmitAsync(responseBuilder);
        }

        public bool IsDialogStarted(DeliverSm data)
        {
            var opParameter = data.Parameters.Of<UssdServiceOpParameter>().FirstOrDefault();
            if (opParameter != null)
            {
                return opParameter.Operation == USSDOperation.PSSRIndication;
            }

            return false;
        }

        public bool IsDialogFinished(DeliverSm data)
        {
            throw new NotImplementedException();
        }

        public bool IsReply(DeliverSm data)
        {
            var opParameter = data.Parameters.Of<UssdServiceOpParameter>().FirstOrDefault();
            if (opParameter != null)
            {
                return opParameter.Operation == USSDOperation.USSRConfirm;
            }

            return false;
        }

        public Task<SubmitSmResp[]> StartDialogAsync(SmeAddress recepient, string text)
        {
            var builder = SMS.ForSubmit()
                .ServiceType(_serviceType)
                .From(_serviceAddress)
                .To(recepient)
                .Text(text);

            builder.AddParameter(new UssdServiceOpParameter(USSDOperation.USSNRequest));

            return _client.SubmitAsync(builder);
        }

        public Task<SubmitSmResp[]> NotifyAsync(SmeAddress recepient, string text)
        {
            var builder = SMS.ForSubmit()
                .ServiceType(_serviceType)
                .From(_serviceAddress)
                .To(recepient)
                .Text(text);

            builder.AddParameter(new UssdServiceOpParameter(USSDOperation.USSRRequest));

            return _client.SubmitAsync(builder);
        }
    }
}